FROM nginx
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY ./src /var/www/html
